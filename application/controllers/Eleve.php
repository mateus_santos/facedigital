<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Eleve extends MY_Controller {

    public function __construct() {
        parent::__construct();       				
    	$this->load->model('ProdutosModel', 'produtoM');    	
        $this->load->model('PedidosModel', 'pedidoM');  
    }
    
    public function index(){
				
       	$parametros['title']    =   "Eleve Software";
        $parametros['dados']    =   $this->produtoM->buscarTodos();
		$this->_load_view_loja('loja/index',$parametros);		
	}

	public function checkout(){
		
    	$parametros             = 	$this->session->userdata();        
        $parametros['title']    =   "Eleve Software - Carrinho de Compras";
		$this->_load_view_loja('loja/checkout',$parametros);
	}

    //Adicionar item na sessão e no carrinho
	public function adicionarItem(){
		 if ($this->input->method() === "get") redirect('pedido');
        $produto_id = $this->input->post('produto_id');
        if(!$this->session->has_userdata('pedido')){            
            //cria o carrinho na sessão
            $pedido['carrinho'] = '';
            //$this->session->set_userdata('pedido', $pedido);
        }
        
        $pedido = $this->session->pedido;
        
        if(isset($pedido['carrinho'][$produto_id])){
            $pedido['carrinho'][$produto_id]['quantidade']++;
            
        }else{
            $produto = $this->produtoM->buscarProdutoId($produto_id);

            if($produto){
                $pedido['carrinho'][$produto_id]['produto_id']  = $produto_id;
                $pedido['carrinho'][$produto_id]['titulo']      = $produto['titulo'];  
                $pedido['carrinho'][$produto_id]['imagem']      = $produto['imagem'];
                $pedido['carrinho'][$produto_id]['valor']       = $produto['valor'];
                $pedido['carrinho'][$produto_id]['quantidade']  = 1;
                
            }else{
                echo json_encode(false);
            }
            
        }

        $this->session->set_userdata('pedido', $pedido);        
        echo json_encode($pedido['carrinho'][$produto_id]);
    }
    
    //Diminuir a qtd de itens selecionados na index e no checkout
    public function diminuirItem(){
        if ($this->input->method() === "get") redirect('pedido');
		
        $produto_id = $this->input->post('produto_id');
        $pedido 	= $this->session->pedido;
        $pedido['carrinho'][$produto_id]['quantidade']--;
        if($pedido['carrinho'][$produto_id]['quantidade'] == 0){
            unset($pedido['carrinho'][$produto_id]);
            $this->session->set_userdata('pedido', $pedido);
            echo json_encode('0');
        }else{
            $this->session->set_userdata('pedido', $pedido);
            echo json_encode($pedido['carrinho'][$produto_id]);
        }
    }

    //Excluir item na sessão e no carrinho(checkout) 
    public function removerItem(){
        if ($this->input->method() === "get") redirect('pedido');
        $produto_id = $this->input->post('produto_id');
        $pedido = $this->session->pedido;
        unset($pedido['carrinho'][$produto_id]);
        $this->session->set_userdata('pedido', $pedido);
        echo json_encode(true);
    }
	
	// Retorna info. do produto pra ser montada a modal com os detalhes do produto
	public function produtoDetalhes()
	{
		$produto_id = $this->input->post('produto_id');
		$produto = $this->produtoM->buscarProdutoId($produto_id);
		echo json_encode(array('produto'=> $produto));
	}

    public function inserirPedido() {
        
        if($this->input->method() === "get") redirect('/');
        if(!$this->session->has_userdata('pedido')) redirect('/');  
        
        $pedido     =   $this->session->userdata('pedido');        
        $usuario    =   $this->input->post();
        
        if( $pedido_id = $this->pedidoM->insere($usuario,$pedido) ){
            $parametros                 =   $this->session->userdata();        
            $parametros['titulo']       =   "Eleve Software - Compra Realizada";
            $parametros['cliente']      =   $this->input->post('nome');
            $parametros['pedido_id']    =   $pedido_id; 
            $parametros['endereco']     =   $usuario;
            $this->_load_view_loja('loja/pedido-finalizado',$parametros);
            $this->session->unset_userdata('pedido');
        }else{
            echo 'erro';
        }
        
    }

    public function pesquisaAvancada()
    {
        $parametros['title']    =   "Eleve Software - Pesquisa de produtos";        
        $this->_load_view_loja('loja/pesquisa-produtos',$parametros);
    }

    public function pesquisarProdutosAjax()
    {
        $filtro = $this->input->post();        
        $retorno = $this->produtoM->buscaProdutosFiltros($filtro);
        $html="";
        foreach($retorno as $dado){    
            $html.='<div class="col-md-4 col-sm-12 produtos" titulo="'.$dado['titulo'].'">
                     <div  class="shopping_box margin-30px-bottom"  >
                         <div class="image position-relative">
                         <img src="'.base_url('bootstrap/images/'.$dado['imagem']).'" alt="'.$dado['titulo'].'" class="img-responsive border-radius" produto_id="'.$dado['id'].'">
                        <div class="overlay">
                            <i class="fa fa-shopping-cart icone-item" onclick="abrirDetalhes('.$dado['id'].')"></i>
                        </div>
                     </div>
                    <div class="shop_content text-center padding-5px-all margin-10px-top descricao_produto" >
                     <span class="text-large text-extra-dark-gray;" produto_id="'.$dado['id'].'" >
                      '.$dado['titulo'].'
                        </span>
                     </div>
                    <div class="shop_content text-center padding-5px-all margin-10px-top">
                        <div class="text-large text-red margin-10px-bottom ">
                         <span>R$ '.number_format($dado['valor'],2,'.',',').'</span>
                            <span class="text-extra-small text-extra-dark-gray "> </span>
                        </div>
                    </div>
                    <div class="shop_content text-center padding-5px-all margin-10px-top botao_confirmar" >
                        <a class="btn btn-red btn-medium text-extra-small margin-10px-bottom add-item" onclick="adiconarItem('.$dado['id'].')">
                            Adicionar ao Carrinho
                        </a>
                    </div>
                    </div>
                </div>';   
        }
        echo json_encode($html);
    }
	
}
?>