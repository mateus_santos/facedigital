<section id="shop" class="text-center">
    <h1 class="display-none" aria-hidden="true">Eleve Software (SEO)</h1>
    <div class="container">
        <div class="row " style="margin-bottom: 20px;">
            <div class="col-lg-6">
                <input type="text" class="form-control" id="titulo-descricao" placeholder="Filtro por titulo ou descrição">
            </div>
            <div class="col-lg-6">                
                <input type="text" class="form-control" id="minimo" placeholder="Preço mínimo" style="    width: 30%;float: left;margin: 0 20px;">
                <input type="text" class="form-control" id="maximo" placeholder="Preço máximo" style="    width: 30%;">
            </div>
        </div>
        <div class="row " style="margin-bottom: 20px;">
            <div class="col-lg-12">
                <a class="btn btn-red" style="text-align: center;" data-toggle="tooltip" data-placement="top" title="" id="pesquisar" data-original-title="Pesquisa Avançada">
                    Pesquisar<i class="fa fa-search" title="Busca Avançada"></i>
                </a>
            </div>            
        </div>
        <div class="row" id="mostrar-produtos">

        </div>        
    </div>
</section>
<!-- shop content end-->