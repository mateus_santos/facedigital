<!-- shop content-->
<section id="shop-cart" class="shop padding-80px-tb">
    <h2 class="display-none" aria-hidden="true">Eleve Software (SEO)</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-12 cart_table wow fadeInUp animated margin-50px-bottom">
                <div class="table-responsive">
                    <table class="table table-bordered border-radius">
                        <thead>
                        <tr>
                            <th class="area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">Produtos</th>
                            <th class="text-center area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">Valor</th>
                            <th class="text-center area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">Quantidade</th>
                            <th class="text-center area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if( count($pedido['carrinho']) == 0 ){ ?>    
                        <tr>
                            <td colspan="5"><center class="text-large">Nenhum produto foi adicionado ao carrinho!</center></td>
                        </tr>

                        <?php }else{ foreach($pedido['carrinho'] as $pedidos){ ?>    
                        <tr produto_id="<?=$pedidos['produto_id']; ?>" class="tr_produtos">
                            <td>
                                <img class="shopping-product" src="<?=base_url('bootstrap/images/'.$pedidos['imagem']); ?>" alt="<?=$pedidos['titulo'];?>">
                                <div class="product-name">
                                    <h6 class="text-large no-margin text-extra-dark-gray"><?=$pedidos['titulo'];?></h6>                                                
                                </div>
                            </td>
                            <td><h6 class="text-large no-margin text-center text-black">R$ <?=number_format($pedidos['valor'],2,'.',',');?></h6></td>
                            <td>
                                <div class="quote text-center">                                                
                                    <h6 class="text-large no-margin text-center text-red">
                                        <i class="fa fa-minus-circle text-red qtd-chekout-<?=$pedidos['produto_id']; ?>" onclick="decreaseAddItem(<?=$pedidos['produto_id']; ?>,'remove');" style="cursor: pointer;" ></i>
                                        <span class="qtd-chekout qtd-<?=$pedidos['produto_id']; ?>">
                                            <?=$pedidos['quantidade']; ?>
                                                
                                        </span>
                                        <i class="fa fa-plus-circle text-red qtd-chekout-<?=$pedidos['produto_id']; ?>" onclick="decreaseAddItem(<?=$pedidos['produto_id']; ?>,'add');" style="cursor: pointer;"></i>
                                    </h6>     
                                </div>
                            </td>
                            <td>
                                <h6 class="text-large no-margin text-center text-black total-<?=$pedidos['produto_id']; ?>" >R$ <?=number_format($pedidos['valor'] * $pedidos['quantidade'],2,'.',',');?></h6>
                            </td>
                            <td class="text-center"><i class="fa fa-times text-red" onclick="removeItem(<?=$pedidos['produto_id']; ?>);" style="cursor: pointer;"></i></td>
                        </tr> 
                        <?php }
                        
                        }
                     ?>
                       

                        </tbody>
                    </table>
                </div>
                <div class="apply_coupon sm-margin-20px-top clearfix">
                    <form action="<?=base_url('eleve/inserirPedido');?>" method="post" id="form_insert">
                    <div class="col-md-12 wow fadeInRight animated">
                        <div class=" totals padding-15px-all xs-padding-5px-all xs-padding-5px-bottom">
                            <h6 class="area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">Confirmação de compra:</h6>
                            
                            <table class="table heading_space">
                                <tbody>
                                <tr>
                                    <td class="texto-endereco">Nome:</td>
                                    <td class="text-red text-large">
                                        <input type="text" class="form-control" name="nome" id="nome" required="required" maxlength="150" />
                                    </td>                                    
                                    <td class="texto-endereco">CPF:</td> 
                                    <td class="text-red text-large">
                                        <input type="text" class="form-control" name="cpf" id="cpf" required="required" />
                                    </td>
                                    <td class="texto-endereco">Email:</td> 
                                    <td class="text-red text-large" colspan="4">
                                        <input type="email" class="form-control" name="email" id="cpf" required="required" maxlength="150"/>
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td class="texto-endereco">Cep:</td> 
                                    <td class=" text-red text-large">
                                        <input type="text" class="form-control" name="cep" id="cep" required="required" />
                                    </td>
                                    <td class="texto-endereco">Endereço:</td>
                                    <td class="text-red text-large">
                                        <input type="text" class="form-control" name="logradouro" id="logradouro" required="required" maxlength="250"/>
                                    </td>                                    
                                    <td class="texto-endereco">Complemento:</td> 
                                    <td class="text-red text-large">
                                        <input type="text" class="form-control" name="complemento" id="complemento" required="required" maxlength="100" />
                                    </td>                                    
                                    <td class="texto-endereco">Cidade:</td> 
                                    <td class="text-red text-large">
                                        <input type="text" class="form-control" name="cidade" id="cidade" required="required" maxlength="100" />
                                    </td>
                                </tr>                                                                
                                </tbody>
                            </table>
                        </div>
                        <div class="row" style="margin-top: 25px;">                                    
                            <div class="col-md-12 col-sm-12 coupon text-right xs-margin-10px-tb xs-text-left">			
                                <a href="<?=base_url('eleve'); ?>" class="btn btn-black btn-large text-small display-inline-block xs-margin-10px-bottom">
                                    <i class="fa fa-arrow-left"></i> Continuar Comprando
                                </a>
                                <?php if( count($pedido['carrinho']) > 0 ){ ?>
									
                                    <button type="submit" class="btn btn-red btn-large text-small display-inline-block xs-margin-10px-bottom" >
                                        <i class="fa fa-check"></i> Fechar Pedido
                                    </button>  
									
                                <?php } ?> 															
                            </div>
                        </div> 
                    </div>
                    </form>  
                </div>
            </div>
        </div>        
    </div>
</section>
<?php	if(isset($erros)){ 
			foreach( $erros as $erro ){  ?>
				<input type="hidden" class="mensagens" value="<?=$erro['mensagem'];?>" />
<?php 		}
		} 	?>
<!-- shop content end-->