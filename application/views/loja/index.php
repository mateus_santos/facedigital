<!-- shop content-->
<section id="shop" class="text-center">
    <h1 class="display-none" aria-hidden="true">Eleve Software (SEO)</h1>
    <div class="container">
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-lg-12">
                <div style="position: absolute;right: 170px;">
                    <input type="text" id="buscar_produtos" placeholder="pesquisar" style="float: right;width: 250px;height: 40px;" >
                </div>
                <div>
                    <a href="<?=base_url('eleve/pesquisaAvancada')?>" class="btn btn-red" style="float: right;" data-toggle="tooltip" data-placement="top" title="Pesquisa Avançada">
                        Busca Avaçada<i class="fa fa-search" title="Busca Avançada"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
        <?php  
			$i=0;
			foreach($dados as $dado){
		?>
			 
            <div class="col-md-4 col-sm-12 produtos" titulo="<?=strtolower($dado['titulo']); ?>">
                <div  class="shopping_box margin-30px-bottom"  >
                    <div class="image position-relative">
                        <img src="<?=base_url('bootstrap/images/'.$dado['imagem']);?>" alt="<?=$dado['titulo'];?>" class="img-responsive border-radius" produto_id="<?=$dado['id'];?>">
                        <div class="overlay">
                            <i class="fa fa-shopping-cart icone-item" onclick="abrirDetalhes(<?=$dado['id'];?>)"  ></i>
                        </div>
                    </div>
                    <div class="shop_content text-center padding-5px-all margin-10px-top descricao_produto" >
                        <span class="text-large text-extra-dark-gray;" produto_id="<?=$dado['id'];?>" >
                        	<?=$dado['titulo']; ?>                        		
                        </span>                                                
                    </div>
					<div class="shop_content text-center padding-5px-all margin-10px-top">
						<div class="text-large text-red margin-10px-bottom ">
							<span>R$ <?=number_format($dado['valor'], 2, ',', '.');?></span>
							<span class="text-extra-small text-extra-dark-gray "> </span>
						</div>
					</div>
					<div class="shop_content text-center padding-5px-all margin-10px-top botao_confirmar" >
						<a class="btn btn-red btn-medium text-extra-small margin-10px-bottom add-item" onclick="adiconarItem(<?=$dado['id'];?>)">
							Adicionar ao Carrinho
						</a>
                    </div>
                </div>
            </div>
		<?php 										
				$i++; 
			} 
		?>
        </div>
    </div>
</section>
<!-- shop content end-->
