
<section id="shop-cart" class="shop padding-80px-tb">
    <h2 class="display-none" aria-hidden="true">Eleve Software (SEO)</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-12 cart_table wow fadeInUp animated margin-50px-bottom">
				 <div class="apply_coupon sm-margin-20px-top clearfix">
                    <div class="col-md-12 wow fadeInRight animated">
                         <div style="text-align: center; margin:0 auto;">
							<img src="<?=base_url('bootstrap/images/ok.png'); 	?>" style="width: 100px;height: 100px;"  />
							<hr/>
						 </div>
						 <div class="row table-resized" style="text-align: center;">
							<h3> <?=$cliente; ?><br/> 							
								Pedido <b>#<?=$pedido_id; ?></b> efetuado com sucesso!
							</h3>
							<hr/>
							<table class="table table-bordered border-radius">
								<thead>
								<tr>
									<th class="area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">Produtos</th>
									<th class="text-center area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">Valor</th>
									<th class="text-center area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">Quantidade</th>
																		
								</tr>
								</thead>
								<tbody>
								<?php $total = 0; foreach($pedido['carrinho'] as $pedido){ ?>    
								<tr produto_id="<?=$pedido['produto_id']; ?>" class="tr_produtos">
									<td style="text-align: left;">
										<img style="width: 35px;height: 35px;" src="<?=base_url('bootstrap/images/'.$pedido['imagem']); ?>" alt="<?=$pedido['titulo'];?>">
										<div class="product-name">
											<h6 class="text-large no-margin text-extra-dark-gray"><?=$pedido['titulo'];?></h6>
										</div>
									</td>
									<td>
										<h6 class="text-large no-margin text-center text-red"><?=number_format($pedido['valor'],2,'.',',');?></h6>
									</td>
									<td>
										<div class="quote text-center">                                                
											<h6 class="text-large no-margin text-center text-red">
												<?=$pedido['quantidade']; ?>												
											</h6>
										</div>
									</td>
																	
								</tr> 
							<?php $total = $total + ($pedido['valor'] * $pedido['quantidade']);	
									} 	
								?> 
								<tr>
									<td colspan="2"></td>
									<td style="background: #f0f0f073;">
										<h6 class="text-large no-margin text-center text-red" >
											<b><?=number_format($total,2,'.',',');?></b>
										</h6>
									</td>
								</tr>
								</tbody>
							</table>
							<hr/>
							<h3>Endereço de Entrega</h3>							
							<div class="col-md-12 wow fadeInRight animated">
								<div class=" totals padding-15px-all xs-padding-5px-all padding-95px-bottom xs-padding-5px-bottom">									
									<table class="table heading_space">
										<tbody>
										<tr>
											<td>Endereço:</td>
											<td class="text-red text-large"><?=$endereco['logradouro']; ?></td>                                    
											<td>Complemento:</td> <td class="text-red text-large"><?=$endereco['complemento']; ?></td>
										</tr>
										<tr>
											<td>Cep:</td> 
											<td class=" text-red text-large"><?=$endereco['cep']; ?></td> 
											<td>Cidade:</td> 
											<td class="text-red text-large"><?=$endereco['cidade']; ?></td>
										</tr>                                
										</tbody>
									</table>
								</div>								
							</div> 
						 </div>
                    </div> 
                </div>	
            </div>
        </div>
        
        
    </div>
</section>
