﻿<!doctype html>
<html class="no-js" lang="pt-br">

<head>    
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1"/>
    <meta name="author" content="Mateus TTU">
    <meta name="description" content="Eleve Software">    
    <meta name="keywords" content="">    
    <title>Eleve Software - <?=$title;?></title>    
	<link rel="stylesheet" href="<?=base_url('bootstrap/css/jquery-ui.min.css'); ?>"/>    	
    <link rel="stylesheet" href="<?=base_url('bootstrap/css/bootstrap.min.css'); ?>"/>    
    <link rel="stylesheet" href="<?=base_url('bootstrap/css/font-awesome.min.css'); ?>"/>        
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/component.css'); ?>"/>    
    <link rel="stylesheet" href="<?=base_url('bootstrap/css/core.css'); ?>"/>
    <link rel="stylesheet" href="<?=base_url('bootstrap/css/style.css'); ?>"/>
</head> 
<?php header('Content-Type: text/html; charset=utf-8'); ?>
<body>

<div id="loader">
	<div class='loader'></div>
</div>
<div id="carregar" style="display: none;">
	<p style="position: absolute;left: 48%;top: 53%;font-size:16px;">Carregando...</p>
</div>

<?php
	// Mostra o carrinho caso hajam itens na sessão
    $total_qtd_carrinho = 0;    
    $mostra = 'display: none;';
	if(isset($pedido['carrinho'])){
		if(isset($pedido['carrinho']) > 0){
			foreach($pedido['carrinho'] as $total){
				$total_qtd_carrinho = $total['quantidade'] + $total_qtd_carrinho;       
			}		
		}	
		$mostra = (count( $pedido['carrinho']) > 0) ?  "display: block;" : "display: none;";
	}	
	
?>
<div id="perspective" class="other perspective effect-moveleft">
    <div class="cont">
        <div class="wrapper">
            <!-- start header -->
            <header>
                <!-- start navigation -->
                <nav class="navbar navbar-default bootsnav navbar-fixed-top nav-white header-light bg-transparent nav-box-width on no-full nav_line">
                    <div class="nav-header-container">
                        <div class="row">                            
                            <div class="carrinho-pontos">
								
                                <div class="carrinho-qtd-prd" style="<?=$mostra;?>">
                                    <p class="carrinho-qtd" total="<?=$total_qtd_carrinho;?>">
                                        <?=$total_qtd_carrinho;?>                                        
                                    </p>
                                </div>
                                <p class="text-small text-blue carrinho-icone"  >
                                    <a href="<?=base_url('Eleve/checkout'); ?>" class="icone-shopping carrinho" title="Carrinho de Compras" >
                                        <i class="fa fa-shopping-cart text-large" ></i>
                                    </a>
                                </p>
                            </div>							
                        </div>
                    </div>
                </nav>
                <!-- Finalizar carrinho -->
                <div id="carrinho-finalizar" class="showMenuButton-two" style="<?=$mostra;?>">
                    <p class="text-small" style="border-bottom: 1px solid #1761ac;text-align: center;font-weight: 600;padding-bottom: 6px;">
                        Ítens do carrinho
                    </p>
                    <table >
                        <?php if(isset($pedido['carrinho'])){                                    
								if(count($pedido['carrinho']) > 0){
                             	  foreach($pedido['carrinho'] as $carrinho){     ?>
                            <tr style="border-bottom: 1px solid #ffcc00;" produto="<?=$carrinho['produto_id']; ?>">
                                <td width="15%" style="padding-bottom: 15px;padding-top: 15px;">
                                    <input type="hidden" class="produto_id_ativo" value="<?=$carrinho['produto_id']; ?>">
                                    <img src="<?=base_url('bootstrap/images/'.$carrinho['imagem']); ?>" class="img-fluid"   />
                                </td>                            
                                <td class="text-extra-small" width="55%" style="text-align: center;">
                                    <?=$carrinho['titulo']; ?>
                                </td>                                
                                <td class="text-extra-small" width="10%" style="text-align: center;">
                                    <i class="fa fa-minus text-extra-small text-black"  onclick="removerQtdItem('<?=$carrinho['produto_id']; ?>');"></i>
                                    <span class="qtd-<?=$carrinho['produto_id']; ?>"> <?=$carrinho['quantidade']; ?> </span>
                                    <input type="hidden" produto_id="<?=$carrinho['produto_id']; ?>" class="produto_qtd_ativo" value="<?=$carrinho['quantidade']; ?>">
                                    <i class="fa fa-plus text-extra-small text-black" onclick="adicionaQtdItem('<?=$carrinho['produto_id']; ?>');"></i>
                                </td>                                   
                            </tr>
                        <?php } 
                            }
						}
					?>
                    </table>
                    <div class="carrinho-finalizar">
                        <a class="btn btn-red btn-large text-lager margin-10px-bottom" href="<?=base_url('eleve/checkout'); ?>" >Finalizar Resgate</a>
                    </div>
                 </div>                
            </header>
        <!-- shop cover-->
        <section class="bg blog-cover" style="">
            <div class="container" style="">
                <div class="text-center" >
                    <a href="<?=base_url('Eleve'); ?>" class="titulo_home">ELEVE SOFTWARE</a>            
                </div>
            </div>
        </section>