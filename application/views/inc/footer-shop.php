 <!-- footer -->
            <footer class="padding-40px-tb bg-red"  >
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="text-small text-white xs-text-center xs-margin-15px-bottom">© <?php echo date('Y'); ?> <a href="#." class="text-white">Eleve Software</a>
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="footer-social text-right xs-text-center xs-margin-10px-top">
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- footer end--> 

            <!-- start scroll to top -->
            <a class="scroll-top-arrow" href="javascript:void(0);"><i class="fa fa-angle-up"></i></a>
            <!-- end scroll to top  -->

        </div>
    </div>
</div>
<script src="<?php echo base_url('bootstrap/js/jquery-3.2.1.js');?>"></script>
<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('bootstrap/js/plugins/jquery-ui.min.js'); ?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo base_url('bootstrap/js/jquery.mask.min.js'); ?>"></script>
<script src="<?php echo base_url('bootstrap/js/main.js');?>"></script>
<?php
    // carregar js particular de cada View
    $js = explode('/', $view); 
    $loadjs = $js[count($js) - 1].'.js';    
?>
<script src="<?=base_url('bootstrap/js/'.$loadjs)?>" type="text/javascript"></script>
</body>
<div class="modal fade" id="m_detalhes" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">                    
                    <div class="col-lg-4 col-sm-12 imagem-detalhes">                        
                        
                    </div>
                    <div class="col-lg-8 col-sm-12 descricao-detalhes">

                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <a class="btn btn-red btn-medium text-extra-small margin-10px-bottom add-item" id="carrinho-detalhes" produto_id="" >Adicionar ao Carrinho</a>
            </div>
        </div>
    </div>
</div>
</html>