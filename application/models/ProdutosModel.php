<?php 

Class ProdutosModel extends CI_Model {
	
    //No formato eloquente      
    public function buscarTodos()
    {		
		$this->db->from('produtos');
		$this->db->order_by("id", "asc");
		$query = $this->db->get();
		return $query->result_array(); 
    }

    //No formato sql
    public function buscarProdutoId($id)
    {
    	$sql = "SELECT *, format(valor, 2, 'de_DE') as valor_formatado
    			FROM produtos 
    			WHERE id=".$id;	
    	return $this->db->query($sql)->row_array();
    }  

    public function buscaProdutosFiltros($filtros)
    {
        $where='1=1';
                
        if($filtros['titulo'] != ''){
            $where.=" and concat(titulo,descricao) like '%".$filtros['titulo']."%'";    
        }
        
        if($filtros['maximo'] != '' && $filtros['minimo'] != ''){
            $maximo = str_replace(',', '.', str_replace('.','',$filtros['maximo']));
            $minimo = str_replace(',', '.', str_replace('.','',$filtros['minimo']));
            $where.=" and valor between '".$minimo."' and '".$maximo."' ";    
        }
        
        $sql = "SELECT * FROM produtos 
                where ".$where;
          
        return $this->db->query($sql)->result_array();
    }    

}

