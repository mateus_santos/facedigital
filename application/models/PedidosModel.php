<?php 

Class PedidosModel extends CI_Model {
	
    public function insere($usuario, $pedido){
       
        $this->db->trans_start();
        $this->db->insert('usuarios', $usuario);
		$inserePedido = array('usuario_id' => $this->db->insert_id());
        $this->db->insert('pedidos', $inserePedido);
        $pedido_id = $this->db->insert_id();
        $total = 0;        
        foreach ($pedido['carrinho'] as $item){
            $salvar_item['pedido_id'] 	= 	$pedido_id;
			$salvar_item['produto_id'] 	= 	$item['produto_id'];			
			$salvar_item['qtd'] 		= 	$item['quantidade'];
            $this->db->insert('pedido_itens', $salvar_item);
            $total = $total + $item['valor'];
        }
        $valor_pedido = array('valor' => $total);
        $this->db->where('id', $pedido_id);
		$this->db->update('pedidos', $valor_pedido); 
        $this->db->trans_complete();        
        return $pedido_id;
    
	}

}
