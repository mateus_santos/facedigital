######################
TESTE - Eleve Software
######################

Foi desenvolvido uma loja virtual contendo produtos fictícios para demonstração do conhecimento no desenvolvimento web.

**********************
Tecnologias Utilizadas
**********************

Back-end: PHP (Framework Codeigniter)
Front-end: HTML (bootstrap), CSS, Javascript(Jquery), Ajax
Banco de Dados: Mysql

**********************
Link para visualização
**********************

http://www.milsistemas.com.br/facedigital

*******************
Vídeo demonstrativo
*******************

https://www.awesomescreenshot.com/video/3737891?key=440065f052771861b6fb6336b058b034

*********************************
Início e Fim do desenvolvimento
*********************************

11/05/2021 a 13/05/2021

*********************************
Horas Trabalhadas
*********************************

14 Horas