// Variável global JS utilizada em todos os códigos js
var base_url = 'http://localhost:8080/facedigital/';
var carrinho;
$(document).ready(function () {
    //Inserção do overlay quando é feito alguma chamada ajax	
	$(document).bind('ajaxStart', function(){
		$('#carregar').attr('style','display: block;');
	}).bind('ajaxStop', function(){
		$('#carregar').attr('style','display: none;');
	});	

	$('[data-toggle="tooltip"]').tooltip(); 
   
    setTimeout(function () {
        $("#loader").fadeOut("slow");
    }, 2000);

    carrinho = setTimeout(function () {
        $('#carrinho-finalizar').fadeOut("slow");
    }, 4000);

    $('#carrinho-finalizar').bind('mouseover', function(){
        clearTimeout(carrinho);         
    });

    $('#carrinho-finalizar').bind('mouseleave', function(){
        carrinho = setTimeout(function () {
            $('#carrinho-finalizar').fadeOut("slow");
        }, 1000);
    });

    $('.carrinho').bind('mouseover', function(){

        if( $('#carrinho-finalizar').find('tr').length > 0 ){
            $('#carrinho-finalizar').fadeIn("slow");
            clearTimeout(carrinho);
        }
    });

    /*================================
      INÍCIO DOS EFEITOS DO TEMPLATE 
    ================================*/
	 "use strict";
    //check for browser os
    var isMobile = false;
    var isiPhoneiPad = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        isMobile = true;
    }

    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        isiPhoneiPad = true;
    }

    /* ===================================
     Header Appear On Scroll
     ====================================== */

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 70) { // Set position from top to add class
            $('header').addClass('sticky header-appear');
            $('.left-logo .navbar-brand').addClass("display_none");
        }
        else {
            $('header').removeClass('sticky header-appear');
            $('.left-logo .navbar-brand').removeClass("display_none");
        }
    });

    // fixing bottom nav to top on scrolliing
    var $fixednav = $(".bottom-nav .navbar-fixed-top");
    $(window).on("scroll", function () {
        var $heightcalc = $(window).height() - $fixednav.height();
        if ($(this).scrollTop() > $heightcalc) {
            $fixednav.addClass("navbar-bottom-top");
        } else {
            $fixednav.removeClass("navbar-bottom-top");
        }
    });

    /* =====================================
              Side Nav Absolute
       ====================================== */

    if ($("body").hasClass("side-nav")) {
        var $menuLeft = $(".pushmenu-left");
        var $menuRight = $(".pushmenu-right");
        var $toggleleft = $(".menu_bars.left");
        var $toggleright = $(".menu_bars.right");
        $toggleright.on("click", function () {
            $('.menu_bars').toggleClass("active");
            $menuRight.toggleClass("pushmenu-open");
            $("body").toggleClass("pushmenu-push-toLeft");
            $(".navbar").toggleClass("navbar-right_open");
            return false;
        });

        $('.push_nav li a').on('click', function () {
            $toggleright.toggleClass("active");
            $menuRight.toggleClass("pushmenu-open");
            $("body").toggleClass("pushmenu-push-toLeft");
            $(".navbar").toggleClass("navbar-right_open");
            return true;
        });
    }
 
    /* =====================================
            Scroll
     ====================================== */

    //scroll to appear
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 150)
            $('.scroll-top-arrow').fadeIn('slow');
        else
            $('.scroll-top-arrow').fadeOut('slow');
    });
    //Click event to scroll to top
    $(document).on('click', '.scroll-top-arrow', function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    //scroll sections
    $(".scroll").on('click', function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 750);
    });

});

//Funções que estão relacionadas à página incial e ao checkout
function removerQtdItem(produto_id){

    $.ajax({
        method: "POST",
        url:  base_url+"Eleve/diminuirItem/", 
        async: true,
        data: { produto_id : produto_id  }
    }).done(function(data) {
        var dados = $.parseJSON(data);      
        
        if(dados)
        {
            novoTotalProduto =  parseInt($('.produto_qtd_ativo[produto_id = "'+produto_id+'"]').val()) - 1;
            novoTotalGeral   =  parseInt($('.carrinho-qtd').attr('total')) - 1;
            
            $('.produto_qtd_ativo[produto_id = "'+produto_id+'"]').val(novoTotalProduto);
            $('span.qtd-'+produto_id).text(novoTotalProduto);    
            $('.carrinho-qtd').attr('total',novoTotalGeral);
            $('.carrinho-qtd').text(novoTotalGeral);
            if(novoTotalProduto == 0){
                $('tr[produto="'+produto_id+'"]').remove();
            }

            if(novoTotalGeral == 0){
                $('#carrinho-finalizar').hide();  
                $('.carrinho-qtd-prd').hide();  
                
            }
        }

    });

}

function adicionaQtdItem(produto_id){
    $.ajax({
        method: "POST",
        url:  base_url+"Eleve/adicionarItem/", 
        async: true,
        data: { produto_id : produto_id  }
    }).done(function(data) {
        var dados = $.parseJSON(data);      
        
        if(dados)
        {			
			novoTotalProduto =  parseInt($('.produto_qtd_ativo[produto_id = "'+produto_id+'"]').val()) + 1;
			novoTotalGeral   =  parseInt($('.carrinho-qtd').attr('total')) + 1;               
			$('.produto_qtd_ativo[produto_id = "'+produto_id+'"]').val(novoTotalProduto);
			$('span.qtd-'+produto_id).text(novoTotalProduto);    
			$('.carrinho-qtd').attr('total',novoTotalGeral);
			$('.carrinho-qtd').text(novoTotalGeral);            
			
        }

    });
    
}

function adiconarItem(produto_id){
    $.ajax({
        method: "POST",
        url:  base_url+"Eleve/adicionarItem/", 
        async: true,
        data: { produto_id : produto_id  }
    }).done(function(data) {
        var dados = $.parseJSON(data);      
        console.log(dados);
        if(dados)
        {
            $('.carrinho-qtd').attr('total', parseInt($('.carrinho-qtd').attr('total')) + 1);
            $('.carrinho-qtd').text($('.carrinho-qtd').attr('total'));
            $('.carrinho-qtd-prd').fadeIn('slow');
            var existe = 0;
            $('.produto_id_ativo').each(function(){
                if( $(this).val() == dados.produto_id ){
                    existe = 1;
                    $('.produto_qtd_ativo[produto_id = "'+dados.produto_id+'"]').val(dados.quantidade);
                    $('span.qtd-'+dados.produto_id).text(dados.quantidade);
                }
            });
            
            if(existe == 0){

                html = '<tr style="border-bottom: 1px solid #ffcc00;" produto="'+dados.produto_id+'">';
                html +=     '<td width="15%" style="padding-bottom: 15px;padding-top: 15px;">';
                html +='                <input type="hidden" class="produto_id_ativo" value="'+dados.produto_id+'">';
                html +='                <img src="'+base_url+'bootstrap/images/'+dados.imagem+'" class="img-fluid"   />';
                html +='            </td>                            ';
                html +='            <td class="text-extra-small" width="55%" style="text-align: center;">';
                html +=             dados.titulo;
                html +='            </td>    ';
                html +='        ';
                html +='            <td class="text-extra-small" width="10%" style="text-align: center;">';
                html +='                <i class="fa fa-minus text-extra-small text-blue"     onclick="removerQtdItem('+dados.produto_id+');"></i>';
                html +='                <span class="qtd-'+dados.produto_id+'"> '+dados.quantidade+' </span>';
                html +='                <input type="hidden" produto_id="'+dados.produto_id+'" class="produto_qtd_ativo" value="'+dados.quantidade+'">';
                html +='                <i class="fa fa-plus text-extra-small text-blue"  onclick="adicionaQtdItem('+dados.produto_id+');"></i>';
                html +='            </td>';                       
                html +='</tr>';

                $('#carrinho-finalizar table').append(html);

            }
            
            $('#carrinho-finalizar').slideDown('Slow');
            
        }

    });

    carrinho = setTimeout(function () {
        $('#carrinho-finalizar').fadeOut("slow");
    }, 3000);
    
}

function abrirDetalhes(produto_id){
    //Abrir detalhes dos produtos 
    $('#carrinho-detalhes').attr('onclick','adiconarItem('+produto_id+')');
    $.ajax({
        method: "POST",
        url:  base_url+"Eleve/produtoDetalhes/", 
        async: true,
        data: { produto_id : produto_id  }
    }).done(function(data) {
        var dados   =   $.parseJSON(data); 
        var imagem  =   ' <img src="'+base_url+'bootstrap/images/'+dados.produto['imagem']+'" />';
        var info    =   ' <h4 class="titulo-detalhes">'+dados.produto['titulo']+'</h4>';
        info        +=  ' <p class="descricao-detalhes">'+dados.produto['descricao']+'</p>';
        info        +=  ' <p class="valor-detalhes">R$ '+dados.produto['valor_formatado']+'</p>';
        $('.imagem-detalhes').empty(); 
        $('.descricao-detalhes').empty();
        $('.imagem-detalhes').append(imagem);            
        $('.descricao-detalhes').append(info);
        $("#m_detalhes").modal({
            show: true
        });
    });


}
