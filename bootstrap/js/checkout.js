$(document).ready(function(){	
  	$('#cep').mask('00000-000');
  	$('#cpf').mask('000.000.000-00');
  	$('#cpf').mask('000.000.000-00');

	$('#carrinho-finalizar').remove();
	$('.carrinho-qtd-prd').remove();
	atualizaSomatorio();			
	$('.mensagens').each(function(){
		swal({
			title: "ATENÇÃO!",
			text: ''+$(this).val()+'',
			icon: "warning",
		}).then(function() {});
	});	
});


function decreaseAddItem(produto_id, tipo){
	var caminho = '';
	if(tipo == 'add'){
		caminho = base_url+"Eleve/adicionarItem/";
	}else{
		caminho = base_url+"Eleve/diminuirItem/";
	}

	$.ajax({
        method: "POST",
        url: caminho, 
        async: true,
        data: { produto_id : produto_id  }
	}).done(function(data) {
        var dados = $.parseJSON(data);      

        if(dados)
        {				
			if(dados == '0'){
				
				$('tr[produto_id ="'+produto_id+'"]').remove();
				if( $('.tr_produtos').length == 0 ){
					$('#fechar_pedido').remove();
				}
			}else{
				novoTotalProduto 	=	dados.quantidade;
				novoValorTotal 		= 	dados.quantidade * dados.valor;
				$('span.qtd-'+produto_id).text(novoTotalProduto);
				$('h6.total-'+dados.produto_id).text(novoValorTotal);
			}

			atualizaSomatorio();
			
        }

    });
}

function removeItem(produto_id){

	$.ajax({
        method: "POST",
        url: base_url+"Eleve/removerItem/", 
        async: true,
        data: { produto_id : produto_id  }
	}).done(function(data) {
        var dados = $.parseJSON(data);      

        if(dados)
        {        	
        	$('tr[produto_id ="'+produto_id+'"]').remove();
        	if( $('.tr_produtos').length == 0 ){
        		$('#fechar_pedido').remove();
        	}
        }

        atualizaSomatorio();

    });
}

function atualizaSomatorio(){
	var somatorio = 0;
	$('.total').each(function(){
		somatorio = parseInt($(this).text()) + somatorio;
		
	});	

	$('.somatorio').text(somatorio);

	if( parseInt($('.pontos_usuario').text()) <  parseInt(somatorio)){
		$('.somatorio').attr('style','color: red');
		$('#fechar_pedido').attr('style','display: none !important;');
		$(".somatorio").notify(
		  "Pontos dos produtos maior que a sua pontuação!", 
		  { position:"right",
		  	className: "warn",
		  	autoHideDelay: 6000 }
		);

	}else{
		$('.somatorio').attr('style','color: #1761ac');
		$('#fechar_pedido').removeAttr('style');
		$('.notifyjs-wrapper').hide();

	}

}