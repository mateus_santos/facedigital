$(document).ready(function(){
    $('#maximo,#minimo').mask("#.##0,00", {reverse: true});
    $('#pesquisar').bind('click', function(){
        var titulo = $('#titulo-descricao').val();
        var minimo = $('#minimo').val();
        var maximo = $('#maximo').val();
        $.ajax({
            method: "POST",
            url:  base_url+"Eleve/pesquisarProdutosAjax/",
            async: true,
            data: { titulo : titulo,
                    minimo : minimo,
                    maximo : maximo  }
        }).done(function(data) {
            var dados = $.parseJSON(data);            
            $('#mostrar-produtos').append(dados);
        });

    });
});