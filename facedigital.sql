-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 13-Maio-2021 às 22:35
-- Versão do servidor: 10.4.18-MariaDB
-- versão do PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `facedigital`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `dthr_criacao` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `pedidos`
--

INSERT INTO `pedidos` (`id`, `usuario_id`, `valor`, `dthr_criacao`) VALUES
(11, 11, '250000.00', '2021-05-13 14:26:55'),
(12, 12, '400000.00', '2021-05-13 14:28:53'),
(13, 13, '400000.00', '2021-05-13 14:30:32'),
(14, 14, '400000.00', '2021-05-13 14:32:14'),
(15, 15, '400000.00', '2021-05-13 14:34:15'),
(16, 16, '150000.00', '2021-05-13 14:36:10'),
(17, 17, '150000.00', '2021-05-13 14:36:21'),
(18, 18, '150000.00', '2021-05-13 14:37:28'),
(19, 19, '150000.00', '2021-05-13 14:39:26'),
(20, 20, '150000.00', '2021-05-13 14:40:15'),
(21, 21, '250000.00', '2021-05-13 15:23:29'),
(22, 22, '100000.00', '2021-05-13 15:25:38'),
(23, 23, '150000.00', '2021-05-13 16:57:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido_itens`
--

CREATE TABLE `pedido_itens` (
  `id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `qtd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `pedido_itens`
--

INSERT INTO `pedido_itens` (`id`, `produto_id`, `pedido_id`, `qtd`) VALUES
(5, 3, 11, 2),
(6, 2, 11, 1),
(7, 3, 12, 1),
(8, 2, 12, 1),
(9, 1, 12, 1),
(10, 3, 13, 1),
(11, 2, 13, 1),
(12, 1, 13, 1),
(13, 3, 14, 1),
(14, 2, 14, 1),
(15, 1, 14, 1),
(16, 1, 15, 2),
(17, 2, 15, 1),
(18, 3, 15, 3),
(19, 1, 16, 3),
(20, 1, 17, 3),
(21, 3, 18, 4),
(22, 1, 19, 1),
(23, 1, 20, 3),
(24, 2, 21, 2),
(25, 3, 21, 1),
(26, 2, 22, 1),
(27, 3, 23, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `dthr_criacao` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `titulo`, `descricao`, `imagem`, `valor`, `dthr_criacao`) VALUES
(1, 'Convallis suspendisse ', 'Convallis suspendisse fringilla nisi maecenas sem aliquam maecenas aenean suspendisse luctus leo condimentum, odio ipsum donec non ligula facilisis massa dui auctor justo. imperdiet lacus mattis maecenas orci vel neque ', '01.png', '150000.00', '2021-05-12 13:44:48'),
(2, 'Senectus risus platea ', 'Senectus risus platea venenatis duis scelerisque placerat viverra quisque praesent auctor, luctus ultrices nisl eu aliquam ornare phasellus ut. lectus ad id condimentum nostra aenean pulvinar posuere vel, non ante proin', '02.png', '100000.00', '2021-05-12 13:44:48'),
(3, 'Dapibus a hac himenaeos', 'Dapibus a hac himenaeos eget tincidunt ut facilisis aenean sem, himenaeos turpis cras nostra euismod rutrum a sit pulvinar in, suspendisse eros erat augue in morbi potenti torquent. per nisi felis arcu', '03.png', '150000.00', '2021-05-12 13:45:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `logradouro` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `complemento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dthr_criacao` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `cpf`, `email`, `cep`, `logradouro`, `cidade`, `complemento`, `dthr_criacao`) VALUES
(1, '', '', '', '', '', '', '', '2021-05-13 13:56:36'),
(2, '', '', '', '', '', '', '', '2021-05-13 13:57:23'),
(3, '', '', '', '', '', '', '', '2021-05-13 13:57:43'),
(4, '', '', '', '', '', '', '', '2021-05-13 13:58:04'),
(5, 'mateus santos', '00871310058', 'mateus.santos@gmail.com', '96085-000', 'avenida ferreira viana, 2962', 'pelotas', 'casa 260', '2021-05-13 14:00:31'),
(9, 'adsasdas', '0000000', 'email@email.combr', 'asdasdad', '21332123', '2123', '121', '2021-05-13 14:14:18'),
(10, 'adsasdas', '0000000', 'email@email.combr', 'asdasdad', '21332123', '2123', '121', '2021-05-13 14:14:45'),
(11, 'mRUWA asaSAD', '0087131100ASD', 'AMTEUSSDASDAD@ASD.COM.BR', '96085000', 'AVENAISDJIAS IOASDJ OIASJIDO JAISODJ IOSAJ IO', 'OIASJDJAOISJDOI', 'AOSIJDAIOSD', '2021-05-13 14:26:55'),
(12, 'mateus santos', '00871310058', 'mateus.santos@gmail.com', '96085000', 'avenida sao francisco de paula', 'pelotas', '260', '2021-05-13 14:28:53'),
(13, 'mateus', '00878897', '9889789897@asdasd.com', '232312323', '321321', '21321321', '3213', '2021-05-13 14:30:32'),
(14, 'mateus', '00878897', '9889789897@asdasd.com', '232312323', '321321', '21321321', '3213', '2021-05-13 14:32:14'),
(15, 'mateus santos', '00871310058', 'mateus.santos@gmail.com', '960850000', 'avenida ferreira vaina', 'pelotas', '260', '2021-05-13 14:34:15'),
(16, 'asdasda', 'asdasdasda', 'asdasda@sadda.com', '96085000', 'avenida ferreira viana', 'cidade', '260', '2021-05-13 14:36:10'),
(17, 'asdasda', 'asdasdasda', 'asdasda@sadda.com', '96085000', 'avenida ferreira viana', 'cidade', '260', '2021-05-13 14:36:21'),
(18, 'asdasda', 'asdasd', 'asdasdasd@asd.com.br', '96085000', 'asdasdasdasd', 'pelotas', '150', '2021-05-13 14:37:28'),
(19, 'asdasd', 'asdasd', 'asadasd@gmail.com', 'asdasd', 'asdasd', 'sdadasd', 'asdasd', '2021-05-13 14:39:26'),
(20, 'qweqweq', 'wqeqweqe', 'ewqeeqe@asdasd.com', 'sadasdasd', 'asdasdasd', 'sdadasdasd', 'asdadasd', '2021-05-13 14:40:15'),
(21, 'sdasda', 'asdasdad', 'asdasdads@fadasd.omc', 'asdasdada', 'asdadasd', 'pelotas', '231121321', '2021-05-13 15:23:29'),
(22, 'sdasdadasd', 'asdadasd', 'asdasdasd@asdasd.com', '564564654', '456456465465', '65465', '6464', '2021-05-13 15:25:38'),
(23, 'MAteus SAntos', '008.713.100-58', 'mateus.santos@gmail.com', '96085-000', 'sadasdasd', '2123asd1231a3d2', 'asdasda', '2021-05-13 16:57:00');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_x_usuario` (`usuario_id`);

--
-- Índices para tabela `pedido_itens`
--
ALTER TABLE `pedido_itens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_itens_x_pedidos` (`pedido_id`),
  ADD KEY `pedido_itens_x_produtos` (`produto_id`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de tabela `pedido_itens`
--
ALTER TABLE `pedido_itens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedido_x_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);

--
-- Limitadores para a tabela `pedido_itens`
--
ALTER TABLE `pedido_itens`
  ADD CONSTRAINT `pedido_itens_x_pedidos` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`),
  ADD CONSTRAINT `pedido_itens_x_produtos` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
